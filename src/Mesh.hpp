#pragma once
#ifndef _MESH_H_
#define _MESH_H_
#include "assimp/Importer.hpp"

class Mesh
{
private:
	unsigned vao;
	vector<unsigned> buffers;

	Mesh();
	Mesh(const Mesh&);

public:
	static unique_ptr<Mesh> MeshFromFile(const string& file);
	static unique_ptr<Mesh> MeshFromScene(const aiScene* scene);

	void Render();
};




#endif