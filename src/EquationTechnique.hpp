#pragma once
#ifndef _EQUATION_TECH_H_
#define _EQUATION_TECH_H_
#include "CompiledHeader.hpp"
#include "Technique.hpp"

class EqTechnique
	: public Technique
{
private:
	unique_ptr<glmesh::Mesh> mesh;
	TwBar* tweakBar;
	unsigned 
		centerPosId,
		maxDisId;


public:
	fvec3 CenterPosition;
	float MaxDistance;

	EqTechnique()
		: Technique("Equation")
	{
		using namespace glutil;
		auto& vertex = UniqueShader(CompileShader(GL_VERTEX_SHADER, 
			GLSL(

		uniform mat4 ProjViewModelMat;
		uniform mat4 ViewModelMat;
		uniform mat4 ModelMat;
		uniform mat3 ViewNormalMat;

		uniform vec3 CenterPos;

		in vec3 position;

		smooth out vec3 FragPos;
		flat out vec3 VertPos;

		void main()
		{
			FragPos = position;
			VertPos = position;
			gl_Position = ProjViewModelMat * vec4(position, 1.0);
		}

		)));

		auto& fragment = UniqueShader(CompileShader(GL_FRAGMENT_SHADER, 
			GLSL(

		uniform mat4 ProjViewModelMat;
		uniform mat4 ViewModelMat;
		uniform mat4 ModelMat;
		uniform mat3 ViewNormalMat;

		uniform vec3 CenterPos;
		uniform float MaxDist;

		smooth in vec3 FragPos;
		flat in vec3 VertPos;

		out vec4 FragColor;

		void main()
		{
			vec3 sqrs = FragPos * FragPos;
			if(length(FragPos) < MaxDist)
				discard;

			FragColor = vec4(1.0f);
		}

		)));


		programId = glCreateProgram();
		Attribs();
		LinkProgram(programId, vertex, fragment);
		Init();
		centerPosId = glGetUniformLocation(programId, "CenterPos");
		maxDisId = glGetUniformLocation(programId, "MaxDist");
		CenterPosition = fvec3(0, 0, 0);
		tweakBar = TwNewBar("Eq - Tech");
		HideBar();
		TwAddVarRW(tweakBar, "Max Distance", TW_TYPE_FLOAT, &MaxDistance, "step=0.01");
		TwAddVarRW(tweakBar, "CenterPos", TW_TYPE_DIR3F, &CenterPosition, NULL);
		mesh.reset(glmesh::gen::Cube());
	}

	void Render()
	{
		Bind();
		ApplyMatrices();
		Uniform(centerPosId, CenterPosition);
		Uniform(maxDisId, MaxDistance);

		mesh->Render();
	}


	void ShowBar()
	{
		TwDefine(" 'Eq - Tech' visible=true ");
	}

	void HideBar()
	{
		TwDefine(" 'Eq - Tech' visible=false ");
	}
};




#endif