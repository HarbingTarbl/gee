#pragma once
#ifndef _COMPILED_HEADER_H_
#define _COMPILED_HEADER_H_

#define USE_GLLOAD

#ifdef USE_GLLOAD
#include <glload\gl_3_0.h>
#include <glload\gl_load.hpp>
#elif defined USE_GLEW 
#define GLEW_STATIC
#include <GL/glew.h>
#else
#error USE_GLLOAD or USE_GLEW not specified
#endif
#include <GLFW/glfw3.h>

#include <glutil/glutil.h>
#include <glimg/glimg.h>
#include <AntTweakBar.h>
#include <glmesh/glmesh.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>


#include <vector>
#include <string>
#include <unordered_map>
#include <iostream>
#include <exception>
#include <memory>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace fi = boost::filesystem;

using glm::fvec2;
using glm::fvec3;
using glm::fvec4;
using glm::fmat3;
using glm::fmat4;
using glm::quat;

using std::string;
using std::vector;
using std::unordered_map;
using std::cout;
using std::endl;
using std::exception;
using std::logic_error;
using std::runtime_error;
using std::unique_ptr;
using std::shared_ptr;

struct CameraData 
{
	fvec3 Position;
	fvec3 Target;
	fvec2 Angle;
	float Zoom;
	bool IsRotating;
	fmat4* ViewMatrix;
};



#define GLSL(x) "#version 130\n"#x

#endif