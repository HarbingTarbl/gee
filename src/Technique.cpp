#include "CompiledHeader.hpp"
#include "Technique.hpp"

fmat4* Technique::ViewMatrix = nullptr;
fmat4* Technique::ModelMatrix = nullptr;
fmat4* Technique::ProjectionMatrix = nullptr;

Technique::~Technique()
{
	glDeleteProgram(programId);
}
