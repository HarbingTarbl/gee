#pragma once

class Effect
{
private:
	unsigned effectId;
	unsigned programId;
	string name;
	unordered_map<string, unsigned> uniforms;

	Effect(const string& name, const string& file);

public:
	~Effect();

	void Bind();

	unsigned Location(const string& name);
	unsigned ProgramId() const;

	void  Uniform(const string& name, const fmat4& v);
	void  Uniform(const string& name, const fmat3& v);
	void  Uniform(const string& name, const fvec4& v);
	void  Uniform(const string& name, const fvec3& v);
	void  Uniform(const string& name, const fvec2& v);
	void  Uniform(const string& name, const float v);
	void Uniform(const string& name, const int v);

	static unique_ptr<Effect> FromFile(const string& name, const string& file);

	static Effect* Current;
};