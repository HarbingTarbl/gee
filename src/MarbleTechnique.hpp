#pragma once
#ifndef _MARBLE_TECH_H_
#define _MARBLE_TECH_H_
#include "CompiledHeader.hpp"
#include "Technique.hpp"


class MarbleTechnique
	: public Technique
{
private:
	unique_ptr<glmesh::Mesh> mesh;
	TwBar* tweakBar;

	unsigned 
		veinScaleId,
		marbleAmpId,
		dimensionalityId;

public:
	float MarbleAmplitude;
	float VeinScale;
	fvec3 Variance;


	MarbleTechnique()
		: Technique("Marble")
	{
		using namespace glutil;
		auto& vertex = UniqueShader(CompileShader(GL_VERTEX_SHADER, 
			GLSL(

			in vec3 position;
			in vec3 normal;

			smooth out vec3 ModelPosition;

			uniform mat4 ProjViewModelMat;
			uniform mat4 ViewModelMat;
			uniform mat4 ModelMat;
			uniform mat3 ViewNormalMat;

			void main()
			{
				ModelPosition = position;
				gl_Position = ProjViewModelMat * vec4(position, 1.0); 

			}


		)));

		auto& fragment = UniqueShader(CompileShader(GL_FRAGMENT_SHADER,
			GLSL(

			vec3 mod289(vec3 x)
			{
			  return x - floor(x * (1.0 / 289.0)) * 289.0;
			}

			vec4 mod289(vec4 x)
			{
			  return x - floor(x * (1.0 / 289.0)) * 289.0;
			}

			vec4 permute(vec4 x)
			{
			  return mod289(((x*34.0)+1.0)*x);
			}

			vec4 taylorInvSqrt(vec4 r)
			{
			  return 1.79284291400159 - 0.85373472095314 * r;
			}

			vec3 fade(vec3 t) {
			  return t*t*t*(t*(t*6.0-15.0)+10.0);
			}

			// Classic Perlin noise
			float cnoise(vec3 P)
			{
			  vec3 Pi0 = floor(P); // Integer part for indexing
			  vec3 Pi1 = Pi0 + vec3(1.0); // Integer part + 1
			  Pi0 = mod289(Pi0);
			  Pi1 = mod289(Pi1);
			  vec3 Pf0 = fract(P); // Fractional part for interpolation
			  vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
			  vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
			  vec4 iy = vec4(Pi0.yy, Pi1.yy);
			  vec4 iz0 = Pi0.zzzz;
			  vec4 iz1 = Pi1.zzzz;

			  vec4 ixy = permute(permute(ix) + iy);
			  vec4 ixy0 = permute(ixy + iz0);
			  vec4 ixy1 = permute(ixy + iz1);

			  vec4 gx0 = ixy0 * (1.0 / 7.0);
			  vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;
			  gx0 = fract(gx0);
			  vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
			  vec4 sz0 = step(gz0, vec4(0.0));
			  gx0 -= sz0 * (step(0.0, gx0) - 0.5);
			  gy0 -= sz0 * (step(0.0, gy0) - 0.5);

			  vec4 gx1 = ixy1 * (1.0 / 7.0);
			  vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;
			  gx1 = fract(gx1);
			  vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
			  vec4 sz1 = step(gz1, vec4(0.0));
			  gx1 -= sz1 * (step(0.0, gx1) - 0.5);
			  gy1 -= sz1 * (step(0.0, gy1) - 0.5);

			  vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);
			  vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);
			  vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);
			  vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);
			  vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);
			  vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);
			  vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);
			  vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);

			  vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
			  g000 *= norm0.x;
			  g010 *= norm0.y;
			  g100 *= norm0.z;
			  g110 *= norm0.w;
			  vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
			  g001 *= norm1.x;
			  g011 *= norm1.y;
			  g101 *= norm1.z;
			  g111 *= norm1.w;

			  float n000 = dot(g000, Pf0);
			  float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
			  float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
			  float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
			  float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
			  float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
			  float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
			  float n111 = dot(g111, Pf1);

			  vec3 fade_xyz = fade(Pf0);
			  vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
			  vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
			  float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x); 
			  return 2.2 * n_xyz;
			}

			

			uniform mat4 ProjViewModelMat;
			uniform mat4 ViewModelMat;
			uniform mat4 ModelMat;
			uniform mat3 ViewNormalMat;

			smooth in vec3 ModelPosition;

			out vec4 FragColor;

			uniform vec3 TileSize = vec3(.5, .5, .5);
			uniform vec3 TilePct = vec3(.98, 1.0, .98);

			vec3 MarbleColor(float x)
			{
				vec3 color;
				x = (x+1)/2;
				for(int i = 0; i < 2; i++)
					x = sqrt(x);
				color = vec3(0.2 + 0.75*x);
				color.b*=0.95;
				return color;
			}

			float MarbleNoise(vec3 v, int freq)
			{
				float val = 0.0;
				float f = 1.0;
				for(int i = 0; i < freq; i++)
				{
					val += abs(cnoise(v * f) / f);
					f *= 2.07;
				}
				return val;
			}

			uniform float MarbleAmplitude = 12.0;
			uniform float VeinScale = 10.25;

			\n#define Integral(x, p, np) ((floor(x)*(p)) + max(fract (x) - (np), 0.0))\n

			void main()
			{
				const int rough = 4;
				vec3 Tile = ModelPosition / TileSize;
				if(fract(Tile.x * .5) > 0.5)
					Tile.z += 0.5;
				vec3 relPos = fract(Tile);
				vec3 tileId = ceil(Tile);
				float m = 3*cnoise(2.1*ModelPosition);
				const float PI = 3.14159265;

				float height = cnoise(ModelPosition);


				//float t = m;
				float t = 2*PI*(ModelPosition.x + m*ModelPosition.z + ModelPosition.y)/TileSize.x;

				t += MarbleAmplitude*MarbleNoise(ModelPosition, rough);
				t = sin(t);
				vec3 marble = MarbleColor(t);
				vec3 fw = fwidth(ModelPosition);
				//vec3 isMarble = smoothstep(vec3(0.0), vec3(0.01, 0, 0.01), relPos);
				//vec3 isMarble = step(relPos, TilePct);
				vec3 isMarble = (Integral(relPos + fw, TilePct, 1 - TilePct) - Integral(relPos, TilePct, 1 - TilePct)) / fw;
				vec3 color = mix(vec3(.2),marble, clamp(isMarble.x*isMarble.z, 0, 1));

				FragColor = vec4(color, 1.0);
			}


		)));

		programId = glCreateProgram();
		Attribs();
		LinkProgram(programId, vertex, fragment);
		Init();
		
		marbleAmpId = Location("MarbleAmplitude");
		veinScaleId = Location("VeinScale");
		dimensionalityId = Location("Dimensionality");

		MarbleAmplitude = 8;
		VeinScale = 5;
		Variance = glm::normalize(fvec3(1));

		tweakBar = TwNewBar("Marble - Tech");
		HideBar();
		TwAddVarRW(tweakBar, "Amplitude", TW_TYPE_FLOAT, &MarbleAmplitude, "");
		TwAddVarRW(tweakBar, "Vein Scale", TW_TYPE_FLOAT, &VeinScale, "");
		TwAddVarRW(tweakBar, "Variance", TW_TYPE_DIR3F, &Variance, "");
		TwAddButton(tweakBar, "Normalize Variance", [](void* client) {
			fvec3* variance = static_cast<fvec3*>(client);
			*variance = glm::normalize(*variance); 
		}, &Variance, "");
		mesh.reset(glmesh::gen::Cube());	
	}

	void Render()
	{
		Bind();
		ApplyMatrices();

		Uniform(marbleAmpId, MarbleAmplitude);
		Uniform(veinScaleId, VeinScale);
		//Uniform(dimensionalityId, Variance);

		mesh->Render();
	}

	void ShowBar()
	{
		TwDefine(" 'Marble - Tech' visible=true ");
	}


	void HideBar()
	{
		TwDefine(" 'Marble - Tech' visible=false ");
	}
};






#endif