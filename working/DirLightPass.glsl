smooth VSData vec2 WorldVertex;
smooth VSData vec2 WorldTexCoord;

uniform sampler2D NormalTexture;
uniform sampler2D DiffuseTexture;
uniform sampler2D DepthTexture;
				
uniform mat4 InverseProjection;
uniform vec3 LightDirection;
uniform vec3 CameraPosition;
				
#ifdef VERTEX_SHADER
layout(location = 0) in vec4 AttribVertex;

void main()
{
	gl_Position = AttribVertex;
	WorldVertex = AttribVertex.xy;
	WorldTexCoord = (AttribVertex.xy + 1) / 2;
}


#elif defined FRAGMENT_SHADER

layout(location = 0) out vec4 Framebuffer;

void main()
{
	vec4 normData = texture(NormalTexture, WorldTexCoord);
	vec4 diffData = texture(DiffuseTexture, WorldTexCoord);
	
	vec3 normal = normData.xyz;
	vec3 color = diffData.xyz;
	
	float specularPower = normData.a;
	float specularCoeff = diffData.a;
	float diffuseCoeff = 1 - specularCoeff;
	
	float depth = texture(DepthTexture, WorldTexCoord).x * 2 - 1;
	vec4 worldPos = (InverseProjection * vec4(WorldVertex, depth, 1));
	worldPos /= worldPos.w;
	
	float diffuseInten = max(diffuseCoeff * dot(LightDirection, normal), 0);
	color *= diffuseInten;
	
	//Camera is always at vec3(0,0,0)
	vec3 toCamera = -normalize(worldPos.xyz);
	vec3 reflection = normalize(reflect(LightDirection, normal));
	
	float specularInten = specularCoeff * pow(clamp(dot(reflection, toCamera), 0, 1), specularPower);
	
		
	Framebuffer.rgb = vec3(1) * (specularInten + diffuseInten);
	Framebuffer.a = 1;
}

#endif