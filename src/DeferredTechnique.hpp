#pragma once
#ifndef _DEFERRED_TECH_H_
#define _DEFERRED_TECH_H_
#include "CompiledHeader.hpp"
#include "Technique.hpp"
#include "Effect.hpp"
#include "Mesh.hpp"


class DeferredTechnique
	: public Technique
{
private:
	unique_ptr<glmesh::Mesh> sphere;
	unique_ptr<glmesh::Mesh> cube;

	
	class Texture2D
	{
	public:
		unsigned textureId;
		unsigned width, height;
		unsigned internalFormat, format;
	
		
		Texture2D(unsigned width, unsigned height, unsigned internalFormat, unsigned format)
			: width(width), height(height), internalFormat(internalFormat), format(format)
		{
			glGenTextures(1, &textureId);
			Bind(0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

		}

		~Texture2D()
		{
			glDeleteTextures(1, &textureId);
		}

		inline void Bind(unsigned slot) const
		{
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindTexture(GL_TEXTURE_2D, textureId);
		}

		void Data(unsigned type, void* data, int level) const
		{
			glTexImage2D(GL_TEXTURE_2D, level, internalFormat, width, height, 0, format, type, data);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, level);
		}



	};

	class GBuffer
	{
	public:
		unsigned framebufferId;
		vector<const Texture2D*> textures;

		enum FramebufferTarget : GLenum
		{
			READ = GL_READ_FRAMEBUFFER,
			DRAW = GL_DRAW_FRAMEBUFFER,
			BOTH = GL_FRAMEBUFFER
		};

		enum FramebufferAttachment : GLenum
		{
			DEPTH = GL_DEPTH_ATTACHMENT,
			STENCIL = GL_STENCIL_ATTACHMENT,
			DEPTHSTENCIL = GL_DEPTH_STENCIL_ATTACHMENT,
			COLOR = GL_COLOR_ATTACHMENT0,
			COLOR0 = GL_COLOR_ATTACHMENT0,
			COLOR1,
			COLOR2,
			COLOR3,
			COLOR4,
			COLOR5,
			COLOR6,
		};

		GBuffer()
		{
			glGenFramebuffers(1, &framebufferId);
		}

		inline void Bind(FramebufferTarget target)
		{
			glBindFramebuffer(target, framebufferId);
		}

		inline void ReadBuffer(FramebufferAttachment attachment)
		{
			glReadBuffer(attachment);
		}

		inline void DrawBuffer(const vector<FramebufferAttachment>& attachments)
		{
			glDrawBuffers(attachments.size(), (GLenum*)attachments.data());
		}

		inline bool IsComplete()
		{
			return GetStatus() == GL_FRAMEBUFFER_COMPLETE;
		}

		inline unsigned GetStatus()
		{
			return glCheckFramebufferStatus(GL_FRAMEBUFFER);
		}

		static const char* GetStatusString(unsigned statusCode)
		{
			static const char* errorStrings[] = {
				"GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT",
				"GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS",
				"GL_FRAMEBUFFER_UNSUPPORTED",
				"GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT",
				"GL_FRAMEBUFFER_COMPLETE",
				"UNKNOWN_STATUS_CODE"
			};

			switch(statusCode)
			{
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
				return errorStrings[0];
			case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
				return errorStrings[1];
			case GL_FRAMEBUFFER_UNSUPPORTED:
				return errorStrings[2];
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
				return errorStrings[3];
			case GL_FRAMEBUFFER_COMPLETE:
				return errorStrings[4];
			}

			return errorStrings[5];
		}

		inline void AttachTexture2D(const Texture2D* texure, FramebufferAttachment point)
		{
			textures.push_back(texure);
			glFramebufferTexture2D(GL_FRAMEBUFFER, point, GL_TEXTURE_2D, texure->textureId, 0);
		}

		static inline void Unbind(FramebufferTarget target)
		{
			glBindFramebuffer(target, 0);
		}

		~GBuffer()
		{
			glDeleteFramebuffers(1, &framebufferId);
		}
	};

	class GBufferPass
	{
	public:
		Texture2D normalTexture;
		Texture2D diffuseTexture;
		Texture2D depthTexture;

		GBuffer gBuffer;

		unique_ptr<Effect> effect;

		GBufferPass(unsigned width, unsigned height)
			: 
			normalTexture(width, height, GL_RGBA16F, GL_RGBA),
			diffuseTexture(width, height, GL_RGBA, GL_RGBA),
			depthTexture(width, height, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT)
		{
			normalTexture.Bind(0);
			normalTexture.Data(GL_FLOAT, nullptr, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			

			diffuseTexture.Bind(0);
			diffuseTexture.Data(GL_FLOAT, nullptr, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			depthTexture.Bind(0);
			depthTexture.Data(GL_FLOAT, nullptr, 0);

			gBuffer.Bind(GBuffer::BOTH);

			gBuffer.AttachTexture2D(&normalTexture, GBuffer::COLOR0);
			gBuffer.AttachTexture2D(&diffuseTexture, GBuffer::COLOR1);
			gBuffer.AttachTexture2D(&depthTexture, GBuffer::DEPTH);
			
			vector<GBuffer::FramebufferAttachment> attachments;
			attachments.emplace_back(GBuffer::COLOR0);
			attachments.emplace_back(GBuffer::COLOR1);
			gBuffer.DrawBuffer(attachments);
			

		
			if(!gBuffer.IsComplete())
				throw runtime_error(string("Incomplete Framebuffer ").append(gBuffer.GetStatusString(gBuffer.GetStatus())));

			gBuffer.Unbind(GBuffer::BOTH);

			effect = Effect::FromFile("Deferred", "DeferredPass.glsl");
		}

		void Apply()
		{
			effect->Bind();
			gBuffer.Bind(GBuffer::BOTH);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		void SpecularCoeff(const float spec)
		{
			effect->Uniform("SpecularCoeff", spec);
		}

		void SpecularPower(const float pow)
		{
			effect->Uniform("SpecularPower", pow);
		}

	};

	class DirectionalLightPass
	{
	public:
		Texture2D* normalTarget;
		Texture2D* diffuseTarget;
		Texture2D* depthTarget;

		unique_ptr<glmesh::Mesh> ffquad;
		unique_ptr<Effect> effect;

		DirectionalLightPass()
		{
			effect = Effect::FromFile("DirLightPass", "DirLightPass.glsl");
			effect->Bind();
			effect->Uniform("DiffuseTexture", 0);
			effect->Uniform("NormalTexture", 1);
			effect->Uniform("DepthTexture", 2);
			ffquad.reset(glmesh::gen::FullScreenQuad());
		}

		void Apply()
		{
			effect->Bind();
			diffuseTarget->Bind(0);
			normalTarget->Bind(1);
			depthTarget->Bind(2);
		}

		void InverseProjection(const fmat4& mat)
		{
			effect->Uniform("InverseProjection", mat);
		}

		void LightDirection(const fvec3& lightDirection)
		{
			effect->Uniform("LightDirection", glm::normalize(lightDirection));
		}

		void Execute()
		{
			GBuffer::Unbind(GBuffer::BOTH);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			ffquad->Render();
		}

		~DirectionalLightPass()
		{

		}


	};


	class PointLightPass
	{


	};


public:
	const unsigned BufferWidth, BufferHeight;
	GBufferPass gBufferPass;
	DirectionalLightPass dirLightPass;

	CameraData* camera;


	DeferredTechnique(unsigned width, unsigned height, CameraData* camera)
		: Technique("Deffered"), 
		BufferWidth(width), BufferHeight(height),
		gBufferPass(width, height),
		camera(camera)
	{


		/// DiffuseTarget = { RGB, SpecularCoeff }
		/// NormalTarget = { XYZ, SpecularPower }

		programId = gBufferPass.effect->ProgramId();
		Init();

		sphere.reset(glmesh::gen::CubePyramid(5));
		dirLightPass.diffuseTarget = &gBufferPass.diffuseTexture;
		dirLightPass.normalTarget = &gBufferPass.normalTexture;
		dirLightPass.depthTarget = &gBufferPass.depthTexture;

		Mesh::MeshFromFile("Violin/Objects/Violin_roble.lwo");
	}

	~DeferredTechnique()
	{

	}

	void Render()
	{
		glEnable(GL_CULL_FACE);

		gBufferPass.Apply();
		ApplyMatrices();
		
		gBufferPass.SpecularCoeff(.4f);
		gBufferPass.SpecularPower(15);
		
		sphere->Render();

		dirLightPass.Apply();
		dirLightPass.effect->Uniform("CameraPosition", camera->Position);
		dirLightPass.InverseProjection(glm::inverse(projectionViewModelMatrix));
		dirLightPass.LightDirection(fvec3(*(camera->ViewMatrix) * fvec4(1, 1, 0, 0)));
		dirLightPass.Execute();


		//gBufferPass.gBuffer.Bind(GBuffer::READ);
		//gBufferPass.gBuffer.ReadBuffer(GBuffer::COLOR0);
		//glBlitFramebuffer(0, 0, BufferWidth, BufferHeight, 0, 0, BufferWidth / 2, BufferWidth / 2, GL_COLOR_BUFFER_BIT, GL_LINEAR);

		//gBufferPass.gBuffer.ReadBuffer(GBuffer::COLOR1);
		//glBlitFramebuffer(0, 0, BufferWidth, BufferHeight, 0, BufferHeight / 2, BufferWidth / 2, BufferHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);

	}

	void ShowBar()
	{

	}

	void HideBar()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

};



#endif