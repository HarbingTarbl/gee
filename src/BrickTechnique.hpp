#pragma once
#ifndef _BRICK_TECH_H_
#define _BRICK_TECH_H_
#include "CompiledHeader.hpp"
#include "Technique.hpp"

class BrickTechnique
	: public Technique
{
private:
	TwBar* tweakBar;
	unsigned 
		lightPosId,
		specCoeffId,
		diffuseCoeffId,
		brickColorId,
		mortarColorId,
		brickSizeId,
		brickPctId,
		ambitIntenId;


public:
	fvec3 LightPosition;
	fvec3 BrickColor;
	fvec3 MortarColor;
	fvec3 BrickSize;
	fvec3 BrickPct;
	float AmbientIten;
	float SpecularCoeff;
	float DiffuseCoeff;
	string CurrentMesh;
	unordered_map<string, unique_ptr<glmesh::Mesh>>* meshes;

	BrickTechnique(unordered_map<string, unique_ptr<glmesh::Mesh>>* meshes)
		: Technique("Brick"), meshes(meshes)
	{
		using namespace glutil;
		auto& fragment = UniqueShader(glutil::CompileShader(GL_FRAGMENT_SHADER, 
			GLSL(

			uniform mat4 ProjViewModelMat;
			uniform mat4 ViewModelMat;
			uniform mat4 ModelMat;
			uniform mat3 ViewNormalMat;

			uniform vec3 BrickColor;
			uniform vec3 MortarColor;
			uniform vec3 BrickSize;
			uniform vec3 BrickPct;


			uniform vec3 LightPos;
			uniform float AmbitInten;
			uniform float SpecCoeff;
			uniform float DiffCoeff;


			smooth in vec3 ModelPos;
			smooth in vec3 ViewNormal;
			smooth in vec3 ViewModelPos;



			out vec4 FragColor;
			void main()
			{
				vec3 tnorm = normalize(ViewNormal);
				vec3 lightDir = normalize(LightPos - ViewModelPos);
				vec3 reflectDir = reflect(-lightDir, tnorm);
				vec3 viewDir = normalize(-ViewModelPos);



				float diffuse = max(dot(lightDir, tnorm), 0.0);
				float spec = 0;
				if(diffuse > 0.0)
				{
					spec = pow(max(dot(reflectDir, viewDir), 0.0), 16);
				}
				float light = min(diffuse * DiffCoeff + SpecCoeff * spec + AmbitInten, 1);


				vec3 pos = ModelPos / BrickSize;

				//if(fract(position.z * 0.5) > 0.5)
				//	position.y += 0.5;

				//if(fract(position.y * 0.5) > 0.5)
				//	position.x += 0.5;

				pos = fract((pos));
				vec3 brick = step(pos, BrickPct);


				vec3 color = mix(MortarColor, BrickColor, brick.x*brick.y*brick.z);
				color = color * light;
				FragColor =  vec4(color, 1.0);
			}
		)));

		auto& vertex = UniqueShader(glutil::CompileShader(GL_VERTEX_SHADER, 
			GLSL(
			in vec3 position;
			in vec3 normal;
			in vec2 uv;
			in vec3 color;

			smooth out vec3 ModelPos;
			smooth out vec3 ViewNormal;
			smooth out vec3 ViewModelPos;

			uniform mat4 ProjViewModelMat;
			uniform mat4 ViewModelMat;
			uniform mat4 ModelMat;
			uniform mat3 ViewNormalMat;




			void main()
			{
				ModelPos = position.xyz;
				ViewModelPos = (ViewModelMat * vec4(position, 1.0)).xyz;
				ViewNormal = ViewNormalMat * normal;
				gl_Position = ProjViewModelMat * vec4(position, 1);
			}
		)));


		programId = glCreateProgram();

		Attribs();

		glutil::LinkProgram(programId, vertex, fragment);

		specCoeffId = glGetUniformLocation(programId, "SpecCoeff");
		diffuseCoeffId = glGetUniformLocation(programId, "DiffCoeff");
		lightPosId = glGetUniformLocation(programId, "LightPos");
		brickSizeId = glGetUniformLocation(programId, "BrickSize");
		brickColorId = glGetUniformLocation(programId, "BrickColor");
		mortarColorId = glGetUniformLocation(programId, "MortarColor");
		brickPctId = glGetUniformLocation(programId, "BrickPct");
		ambitIntenId = glGetUniformLocation(programId, "AmbitInten");

		Init();
		CurrentMesh = "cube";
		SpecularCoeff = 0.0f;
		DiffuseCoeff = 1.0f;
		LightPosition = fvec3(0, 0, -5);
		MortarColor = fvec3(1, 1, 1);
		BrickColor = fvec3(.4, .2, .1);
		BrickSize = fvec3(.5, .5, .5);
		BrickPct = fvec3(.8, .8, .8);
		AmbientIten = 0.05;



		tweakBar = TwNewBar("Brick - Tech");
		HideBar();
		TwAddVarRW(tweakBar, "DiffuseCoeff", TW_TYPE_FLOAT, &DiffuseCoeff, NULL);
		TwAddVarRW(tweakBar, "SpecularCoeff", TW_TYPE_FLOAT, &SpecularCoeff, NULL);
		TwAddVarRW(tweakBar, "Light Position", TW_TYPE_DIR3F, &LightPosition, NULL);
		TwAddVarRW(tweakBar, "Current Mesh", TW_TYPE_STDSTRING, &CurrentMesh, NULL);
		TwAddVarRW(tweakBar, "BrickSize", TW_TYPE_DIR3F, &BrickSize.x, NULL);
		TwAddVarRW(tweakBar, "BrickPct", TW_TYPE_DIR3F, &BrickPct.x, NULL);
		TwAddVarRW(tweakBar, "BrickColor", TW_TYPE_COLOR3F, &BrickColor, NULL);
		TwAddVarRW(tweakBar, "MortarColor", TW_TYPE_COLOR3F, &MortarColor, NULL);
		TwAddVarRW(tweakBar, "Ambient Light", TW_TYPE_FLOAT, &AmbientIten, NULL);
	}


	void Render()
	{
		auto find = meshes->find(CurrentMesh);
		if(find == meshes->end())
			return;

		Bind();
		ApplyMatrices();
		Uniform(specCoeffId, SpecularCoeff);
		Uniform(diffuseCoeffId, DiffuseCoeff);
		Uniform(lightPosId, fvec3(*ViewMatrix * fvec4(LightPosition, 1.0)));
		Uniform(brickColorId, BrickColor);
		Uniform(mortarColorId, MortarColor);
		Uniform(brickSizeId, BrickSize);
		Uniform(brickPctId, BrickPct);
		Uniform(ambitIntenId, AmbientIten);

		find->second->Render();
	}



	void ShowBar()
	{
		TwDefine(" 'Brick - Tech' visible=true ");
		TwSetTopBar(tweakBar);
		TwSetBottomBar(tweakBar);
	}

	void HideBar()
	{
		TwDefine(" 'Brick - Tech' visible=false ");
	}
};


#endif