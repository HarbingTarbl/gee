#pragma once
#ifndef _TECHNIQUE_H_
#define _TECHNIQUE_H_

class Technique
{
protected:
	unsigned 
		programId,
		projViewModelMatId,
		viewModelMatId,
		modelMatId,
		normalMatId;

	string name;
	fmat4 projectionViewModelMatrix;

	Technique(const string& name)
		: name(name)
	{

	}

	template<typename T>
	class Uni
	{
		Uni<T>(const string& name)
		{
			id = Location(name);
		}

		
		T value;
		unsigned id;
	};

	void Init()
	{
		if(programId == -1)
		{
			throw logic_error("Invalid programId in technique " + name);
		}

		
		

		projViewModelMatId = glGetUniformLocation(programId, "ProjViewModelMat");
		viewModelMatId = glGetUniformLocation(programId, "ViewModelMat");
		modelMatId = glGetUniformLocation(programId, "ModelMat");
		normalMatId = glGetUniformLocation(programId, "ViewNormalMat");
	}

	void Attribs()
	{
		glBindAttribLocation(programId, 0, "position");
		glBindAttribLocation(programId, 2, "normal");
		glBindAttribLocation(programId, 5, "uv");
		glBindAttribLocation(programId, 1, "color");
	}

	void Bind()
	{
		glUseProgram(programId);
	}

	unsigned Location(const string& name)
	{
		return glGetUniformLocation(programId, name.c_str());
	}

	void ApplyMatrices()
	{
		projectionViewModelMatrix = *ModelMatrix;
		Model(projectionViewModelMatrix);
		projectionViewModelMatrix = *ViewMatrix * projectionViewModelMatrix;
		ViewModel(projectionViewModelMatrix);
		ViewNormal(glm::inverse(glm::transpose(fmat3(projectionViewModelMatrix))));
		projectionViewModelMatrix = *ProjectionMatrix * projectionViewModelMatrix;
		ProjViewModel(projectionViewModelMatrix);
	}
	
public:
	static fmat4* ViewMatrix;
	static fmat4* ProjectionMatrix;
	static fmat4* ModelMatrix;
	
	virtual void Render() = 0;
	virtual void ShowBar() = 0;
	virtual void HideBar() = 0;
	virtual ~Technique();

	inline void Uniform(const unsigned id, const fmat3& mat) const
	{
		if(id == -1)
			return;

		glUniformMatrix3fv(id, 1, false, glm::value_ptr(mat));
	}

	inline void Uniform(const unsigned id, const fmat4& mat) const
	{
		if(id == -1)
			return;

		glUniformMatrix4fv(id, 1, false, glm::value_ptr(mat));
	}

	inline void Uniform(const unsigned id, const fvec3& mat) const
	{
		if(id == -1)
			return;

		glUniform3fv(id, 1, glm::value_ptr(mat));
	}

	inline void Uniform(const unsigned id, const fvec2& mat) const
	{
		if(id == -1)
			return;

		glUniform2fv(id, 1, glm::value_ptr(mat));
	}

	inline void Uniform(const unsigned id, const float v) const
	{
		if(id == -1)
			return;

		glUniform1f(id, v);
	}

	inline void Uniform(const unsigned id, const int v) const
	{
		if(id == -1)
			return;

		glUniform1i(id, v);
	}

	void ProjViewModel(const fmat4& mat) const
	{
		Uniform(projViewModelMatId, mat);
	}

	void ViewModel(const fmat4& mat) const
	{
		Uniform(viewModelMatId, mat);
	}

	void Model(const fmat4& mat) const
	{
		Uniform(modelMatId, mat);
	}

	void ViewNormal(const fmat3& mat) const
	{
		Uniform(normalMatId, mat);
	}
};


#endif