#pragma once
#include "CompiledHeader.hpp"
#include "Technique.hpp"
#include <random>

class BoidsTechnique
	: public Technique
{

private:
	unsigned displayShader;

	unsigned boidVertexArray;
	unsigned boidVertexBuffer;
	unsigned boidPrimSize;

	unsigned boidDataBuffer;

	unsigned timeDeltaLocation;
	unsigned signWaveLocation;

	fmat4* oldProj;
	fmat4 projMatrix;

	class Boid
	{
	public:
		fvec3 Position;
		fvec3 Direction;

		Boid()
			: Position(0), Direction()
		{

		}

		Boid(const fvec3& pos)
			: Position(pos), Direction()
		{

		}

		Boid(const fvec3& pos, const fvec3& dir)
			: Position(pos), Direction(dir)
		{

		}
	};

	vector<Boid> boids;
	vector<fvec3> boidNewHeading;

	double oldTime, newTime, deltaTime;

	void drawBoids()
	{
		Uniform(timeDeltaLocation, (float)(deltaTime));
		Uniform(signWaveLocation, (float)std::abs(std::sin(newTime)));

		glBindVertexArray(boidVertexArray);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glBindBuffer(GL_ARRAY_BUFFER, boidDataBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Boid) * boids.size(), nullptr, GL_STREAM_DRAW);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Boid) * boids.size(), boids.data(), GL_STREAM_DRAW);
	
		glDrawArraysInstancedARB(GL_TRIANGLES, 0, boidPrimSize, boids.size());
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}


	void updateBoids()
	{
		//Seperation
		for(int i = 0; i < boids.size(); i++)
		{
			auto& b = boids[i];

			fvec3 toCenter(0);
			fvec3 preventCollide(0);
			fvec3 matchSpeed(0);
			int nearby = 1;
			int collideNearby =1 ;


			for(int j = 0; j < boids.size(); j++)
			{
				auto &ob = boids[j];
				if(&b == &ob)
					continue;

				auto dist = glm::distance2(b.Position, ob.Position);
				auto oneover = 1.0f/dist;
				if(dist < 100 && dist > 70  || dist < 25 && dist > 0)
				{
					preventCollide += (oneover* (b.Position - ob.Position));
					collideNearby++;
				}
				else if(dist <=70  && dist >= 25)
				{
					preventCollide -= (dist / 500.0f) * (b.Position - ob.Position);
					matchSpeed += ob.Direction;
					nearby++;
				}

				//toCenter += ob.Position;
			}


			toCenter /= (boids.size() - 1);
			matchSpeed /= nearby;

			toCenter = b.Position - toCenter;

			b.Direction = glm::normalize(30.0f * b.Direction  + matchSpeed * 10.0f + 10.0f * preventCollide - toCenter / 100.0f);
			b.Position += b.Direction * (float)deltaTime * 50.0f;
		}


		//for(int i = 0; i < boids.size(); i++)
		//{
		//	boids[i].Direction = boidNewHeading[i];
		//	boids[i].Position += boids[i].Direction * (float)deltaTime * 5.0f;
		//}
	}


public:
	BoidsTechnique()
		: Technique("Boids")
	{
		using namespace glutil;

		auto dispFrag = UniqueShader(CompileShader(GL_FRAGMENT_SHADER, 
			GLSL(
			uniform float SinWave;
			uniform float TimeDelta;
			in vec3 fragPos;
			in vec3 fragWeight;

			out vec4 outColor;
			void main()
			{
				outColor = vec4(1);
			}
			)));

		auto dispVertex = UniqueShader(CompileShader(GL_VERTEX_SHADER,
			GLSL(

			uniform float SinWave;
			uniform float TimeDelta;


			uniform mat4 ProjViewModelMat;
			uniform mat4 ViewModelMat;
			uniform mat4 ModelMat;
			uniform mat3 ViewNormalMat;


			in vec3 AttribPosition;
			in vec3 AttribWeight;

			in vec3 InstancePosition;
			in vec4 InstanceDirection;

			vec3 rotate_vector(in vec4 quat, in vec3 vec )
			{
				return vec + 2.0 * cross( cross( vec, quat.xyz ) + quat.w * vec, quat.xyz );
			}

			void main()
			{
				vec3 translatedPosition = AttribPosition + InstancePosition * 1;
			
				gl_Position = ProjViewModelMat * vec4(translatedPosition, 1);
			}
			)));

		programId = displayShader = glCreateProgram();
		glBindAttribLocation(displayShader, 0, "AttribPosition");
		glBindAttribLocation(displayShader, 1, "AttribWeight");

		glBindAttribLocation(displayShader, 2, "InstancePosition");
		glBindAttribLocation(displayShader, 3, "InstanceDirection");

		LinkProgram(displayShader, dispFrag, dispVertex);
		Init();

		signWaveLocation = Location("SinWave");
		timeDeltaLocation = Location("TimeDelta");

		glGenVertexArrays(1, &boidVertexArray);
		glGenBuffers(1, &boidVertexBuffer);
		glGenBuffers(1, &boidDataBuffer);


		fvec3 positionData[] = {
			fvec3(-0.594108, -1.0, 0.594108),
			fvec3(-0.594108, -1.0, -0.594108),
			fvec3(0.594108, -1.0, -0.594108),
			fvec3(0.594108, -1.000000, 0.594108),
			fvec3(0.000000, 1.000000, 0.000000),
		};

		unsigned char elements[] = {
			0, 1, 2,
			1, 0, 4,
			4, 2, 1,
			4, 3, 2, 
			4, 0, 3,
			3, 0, 2
		};

		boidPrimSize = std::distance(std::begin(elements), std::end(elements));
		glBindBuffer(GL_ARRAY_BUFFER, boidVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, boidPrimSize * sizeof(fvec3), nullptr, GL_STATIC_DRAW);
		auto posAttribBuf = static_cast<fvec3*>(glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
		for(auto& element : elements)
		{
			*posAttribBuf++ = positionData[element];
		}
		glUnmapBuffer(GL_ARRAY_BUFFER);

		glBindVertexArray(boidVertexArray);
		glBindBuffer(GL_ARRAY_BUFFER, boidVertexBuffer);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);


		glBindBuffer(GL_ARRAY_BUFFER, boidDataBuffer);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glVertexAttribPointer(2, 3, GL_FLOAT, false, sizeof(Boid), (void*)offsetof(Boid, Position));
		glVertexAttribPointer(3, 3, GL_FLOAT, false, sizeof(Boid), (void*)offsetof(Boid, Direction));

		glVertexAttribDivisorARB(2, 1);
		glVertexAttribDivisorARB(3, 1);

		glBindVertexArray(0);

		projMatrix = glm::perspective(45.0f, 1024.0f/960.0f, 40.0f, 10000.0f);



		boids.reserve(50);

		std::default_random_engine engine;
		std::uniform_real_distribution<float> dist(0, 1);

		for(int x = -10; x <= 10; x++)
		{
			for(int y = -5; y <= 5; y++)
			{
				boids.emplace_back(10.0f * fvec3(x * 1.2, 0, y * 1.2), (fvec3(dist(engine), dist(engine), dist(engine))));
			}
		}

		boidNewHeading.resize(boids.size());


	}

	void Render()
	{
		glDisable(GL_CULL_FACE);
		Bind();
		ApplyMatrices();
		newTime = glfwGetTime();
		deltaTime = newTime - oldTime;
		oldTime = newTime;
		drawBoids();
		updateBoids();
	}

	void ShowBar()
	{
		newTime = oldTime = glfwGetTime();
		oldProj = Technique::ProjectionMatrix;
		Technique::ProjectionMatrix = &projMatrix;
	}

	void HideBar()
	{
		glEnable(GL_CULL_FACE);
		fmat4 swap(projMatrix);
		Technique::ProjectionMatrix = oldProj;
	}

};