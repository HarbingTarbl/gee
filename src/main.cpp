#include "CompiledHeader.hpp"
#include "BrickTechnique.hpp"
#include "EquationTechnique.hpp"
#include "MarbleTechnique.hpp"
#include "Boids.hpp"
#include "DeferredTechnique.hpp"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

using glmesh::StreamBuffer;


GLFWwindow *MainWindow, *AntWindow;
TwBar* AntBar;
glmesh::Mesh* CMesh;
Technique* CTech;
unordered_map<string, unique_ptr<Technique>> techniques;
unordered_map<string, unique_ptr<glmesh::Mesh>> meshes;
string MeshName;
string TechName;
glm::quat ModelOrin;
glm::quat CameraOrin;
fvec3 ModelPos;
fvec3 ModelScale;

fmat4 ModelMatrix;
fmat4 ViewMatrix;
fmat4 ProjMatrix;

CameraData Camera;



void RotateCamera(double dx, double dy)
{
	Camera.Angle.x += dx;
	Camera.Angle.y += dy;
}

void UpdateCamera()
{
	Camera.Target = ModelPos;
	fvec3 zoomedPos = Camera.Position - Camera.Zoom*glm::normalize(Camera.Position - Camera.Target);
	ViewMatrix = glm::lookAt(zoomedPos, Camera.Target, fvec3(0, 1, 0)) * glm::eulerAngleXY(-Camera.Angle.y, -Camera.Angle.x);
}




void InitalizeOpenGL()
{
#ifdef USE_GLEW
	glewExperimental = true;
	auto loaded = glewInit();

	if(loaded != GLEW_OK)
		throw runtime_error("Could not load OpenGL Context (GLEW)");

	int major, minor;

	cout << "Loaded " << glGetString(GL_VERSION) << " GLEW" <<  endl;
#elif defined USE_GLLOAD
	auto& loaded = glload::LoadFunctions();
	if(!loaded)
		throw runtime_error("Could not load OpenGL Context (GLLoad)");

	cout << "Loaded " << glGetString(GL_VERSION) << " GlLoad" << endl;
#endif
}



void RenderMainWindow()
{
	int windowWidth, windowHeight;
	glfwGetWindowSize(MainWindow, &windowWidth, &windowHeight);
	glViewport(0, 0, windowWidth, windowHeight);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	UpdateCamera();
	ModelMatrix = glm::translate(ModelPos) * glm::toMat4(ModelOrin) * glm::scale(ModelScale);
	if(CTech)
		CTech->Render();
	

	glfwSwapBuffers(MainWindow);
}


void RenderAntWindow()
{
	int windowWidth, windowHeight;
	glfwGetWindowSize(AntWindow, &windowWidth, &windowHeight);
	glViewport(0, 0, windowWidth, windowHeight);
	TwWindowSize(windowWidth, windowHeight);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	TwDraw();
	glfwSwapBuffers(AntWindow);
}


void TW_CALL CopyString(string& lhs, const string& rhs)
{
	lhs = rhs;
}

int main(int argc, char* args)
{
	unique_ptr<StreamBuffer> streamBuffer;
	try
	{
		glfwInit();
		TwInit(TW_OPENGL, NULL);

		glfwWindowHint(GLFW_RESIZABLE, 0);
		glfwWindowHint(GLFW_DECORATED, 0);
		AntWindow = glfwCreateWindow(600, 800, "Gee - Debug", NULL, NULL);
		glfwWindowHint(GLFW_ALPHA_BITS, 8);
		glfwWindowHint(GLFW_BLUE_BITS, 8);
		glfwWindowHint(GLFW_RED_BITS, 8);
		glfwWindowHint(GLFW_GREEN_BITS, 8);
		glfwWindowHint(GLFW_DEPTH_BITS, 24);
		MainWindow = glfwCreateWindow(1024, 960, "Gee", NULL, MainWindow);
		glfwSetWindowPos(MainWindow, 50, 50);
		int windowX, windowY, windowWidth, windowHeight;
		glfwGetWindowPos(MainWindow, &windowX, &windowY);
		glfwGetWindowSize(MainWindow, &windowWidth, &windowHeight);
		glfwSetWindowPos(AntWindow, windowX + windowWidth, windowY);

		glfwMakeContextCurrent(MainWindow);
		InitalizeOpenGL();
		streamBuffer.reset(new StreamBuffer(2048));
		meshes.emplace("cube", std::unique_ptr<glmesh::Mesh>(glmesh::gen::Cube()));
		meshes.emplace("cube3", std::unique_ptr<glmesh::Mesh>(glmesh::gen::CubeBlock(3)));
		meshes.emplace("cubepy", std::unique_ptr<glmesh::Mesh>(glmesh::gen::CubePyramid(3)));
		meshes.emplace("sphere", std::unique_ptr<glmesh::Mesh>(glmesh::gen::UnitSphere(32, 32)));
		Technique::ModelMatrix = &ModelMatrix;
		Technique::ViewMatrix = &ViewMatrix;
		Technique::ProjectionMatrix = &ProjMatrix;
		techniques.emplace("Brick", unique_ptr<Technique>(new BrickTechnique(&meshes)));
		techniques.emplace("Eq", unique_ptr<Technique>(new EqTechnique()));
		techniques.emplace("Marble", unique_ptr<Technique>(new MarbleTechnique()));
		techniques.emplace("Boid", unique_ptr<Technique>(new BoidsTechnique()));
		techniques.emplace("Def", unique_ptr<Technique>(new DeferredTechnique(windowWidth, windowHeight, &Camera)));
		glClearColor(0, 0, 0, 1);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		glfwMakeContextCurrent(AntWindow);
		glClearColor(0, 0, 0, 1);
		TechName = "Boid";
		CTech = techniques["Boid"].get();
		CTech->ShowBar();

		AntBar = TwNewBar("Model");
		TwCopyStdStringToClientFunc(CopyString);
		TwAddVarRW(AntBar, "ModelOrin", TW_TYPE_QUAT4F, &ModelOrin, "");
		TwAddVarRW(AntBar, "ModelScale", TW_TYPE_DIR3F, &ModelScale, "");
		TwAddVarRW(AntBar, "ModelPos", TW_TYPE_DIR3F, &ModelPos, "");
		TwAddVarCB(AntBar, "CTech", TW_TYPE_STDSTRING, 
			static_cast<void(TW_CALL*)(const void*, void*)>([](const void* value, void*) { 
			TechName = *static_cast<const string*>(value);
			if(CTech)
					CTech->HideBar();
			auto find = techniques.find(TechName);
			if(find != techniques.end())
			{
				CTech = find->second.get();
				CTech->ShowBar();
			}
			else
			{
				CTech = nullptr;
			}
		}),	static_cast<void(TW_CALL*)(void*, void*)>([](void* value, void*) {
			TwCopyStdStringToLibrary(*static_cast<string*>(value), TechName);
		}), NULL, NULL);

		glfwSetCursorPosCallback(AntWindow, [](GLFWwindow* window, double x, double y) {
			TwEventMousePosGLFW((int)x, (int)y);
		});
		glfwSetMouseButtonCallback(AntWindow, [](GLFWwindow* window, int b, int a, int m) {
			TwEventMouseButtonGLFW(b, a);
		});
		glfwSetKeyCallback(AntWindow, [](GLFWwindow* window, int k, int s, int a, int m) {
			TwEventKeyGLFW(k, a);
		});
		glfwSetCharCallback(AntWindow, [](GLFWwindow* window, unsigned uni) {
			TwEventCharGLFW(uni, 1);
		});

		glfwSetMouseButtonCallback(MainWindow, [](GLFWwindow* window, int b, int a, int m) {
			if(b == GLFW_MOUSE_BUTTON_LEFT)
			{
				if(a == GLFW_PRESS)
				{
					Camera.IsRotating = true;
				}
				else
				{
					Camera.IsRotating = false;
				}
			}
			else if(b == GLFW_MOUSE_BUTTON_RIGHT)
			{
				if(a == GLFW_PRESS)
				{

				}
				else
				{

				}
			}
		});

		glfwSetCursorPosCallback(MainWindow, [](GLFWwindow* window, double x, double y) {
			static double lx = x, ly = y;
			if(Camera.IsRotating)
				RotateCamera((x - lx)/1e2, (y - ly)/1e2);
			lx = x;
			ly = y;
		});

		glfwSetScrollCallback(MainWindow, [](GLFWwindow* window, double x, double y) {
			static double lx = x, ly = y;
			Camera.Zoom += y;
			lx = x;
			ly = y;
		});

		TwDefine(" 'Model' fontsize=3 valueswidth=fit ");


		ProjMatrix = glm::perspective(45.0f, 1024.0f/960.0f, 2.0f, 50.0f);
		ModelPos = fvec3(0,0,0);
		ModelScale = fvec3(1, 1, 1);
		Camera.Position = fvec3(0, 0, -5);
		Camera.Target = fvec3(0, 0, 0);
		Camera.ViewMatrix = &ViewMatrix;

	}
	catch(exception& e)
	{
		SetForegroundWindow(GetConsoleWindow());
		cout << e.what() << endl;
		std::getchar();
		return 1;
	}

	while(true)
	{
		glfwPollEvents();
		if(glfwWindowShouldClose(MainWindow) 
			|| glfwWindowShouldClose(AntWindow) 
			|| glfwGetKey(MainWindow, GLFW_KEY_ESCAPE)
			|| glfwGetKey(AntWindow, GLFW_KEY_ESCAPE))
		{
			TwDeleteBar(AntBar);
			TwTerminate();
			glfwSetWindowShouldClose(MainWindow, true);
			glfwSetWindowShouldClose(AntWindow, true);
			glfwDestroyWindow(AntWindow);
			glfwDestroyWindow(MainWindow);
			glfwTerminate();
			break;
		}
	
		glfwMakeContextCurrent(MainWindow);
		RenderMainWindow();
		glfwMakeContextCurrent(AntWindow);
		RenderAntWindow();
	}

	return 0;
}