#include "CompiledHeader.hpp"
#include "Mesh.hpp"

#include "assimp/scene.h"
#include "assimp/postprocess.h"

unique_ptr<Mesh> Mesh::MeshFromScene(const aiScene* scene)
{
	for(auto i = 0; i < scene->mNumMeshes; i++)
	{
		
	}

	for(auto i = 0; i < scene->mNumMaterials; i++)
	{
		auto material = scene->mMaterials[i];
		
		for(int k = 0; k < material->mNumProperties; k++)
		{
			auto property = material->mProperties[k];

			cout << property->mKey.C_Str() << " : ";
			switch(property->mType)
			{
			case aiPropertyTypeInfo::aiPTI_String:
				cout << ((aiString*)property->mData)->C_Str();
				break;
			case aiPropertyTypeInfo::aiPTI_Float:
				cout << std::to_string(*(float*)property->mData);
				break;
			case aiPropertyTypeInfo::aiPTI_Integer:
				cout << std::to_string(*(int*)property->mData);
				break;
			default:
				cout << "Uknown Type";
				break;
			}

			cout << endl;
		}

		auto print = [material](aiTextureType texture) {
			for(auto k = 0; k < material->GetTextureCount(texture); k++)
			{
				aiString str;
				material->GetTexture(texture, k, &str);
				cout << "\t" << str.C_Str() << endl;
			}
		};

		for(auto k = (int)aiTextureType_NONE; k < aiTextureType_UNKNOWN; k++)
		{
			if(material->GetTextureCount((aiTextureType)k) == 0)
				continue;

			cout << k << " : "  << endl;
			print((aiTextureType)k);
			cout << endl << endl;
		}


	}

	return unique_ptr<Mesh>();
}


unique_ptr<Mesh> Mesh::MeshFromFile(const string& file)
{
	using namespace Assimp;

	Importer import;
	const aiScene* scene = import.ReadFile(file, 
		aiProcess_CalcTangentSpace | aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_Triangulate | aiProcess_OptimizeGraph);

	return MeshFromScene(scene);
}