smooth VSData vec2 WorldTexCoord;
smooth VSData vec3 WorldPosition;
smooth VSData vec3 WorldNormal;

uniform mat4 ProjViewModelMat; 
uniform mat4 ViewModelMat; 
uniform mat4 ModelMat; 
uniform mat3 ViewNormalMat; 

uniform float SpecularCoeff;
uniform float SpecularPower;
							
				
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 AttribVertex;
layout(location = 2) in vec3 AttribNormal;
layout(location = 5) in vec2 AttribTexCoord;

void main()
{
	gl_Position = ProjViewModelMat * vec4(AttribVertex, 1);
	WorldPosition = (ViewModelMat * vec4(AttribVertex, 1)).xyz;
	WorldTexCoord = AttribTexCoord;
	WorldNormal =(ViewNormalMat * AttribNormal);
}


#elif defined FRAGMENT_SHADER


layout(location = 1) out vec4 DiffuseTarget;
layout(location = 0) out vec4 NormalTarget;

void main()
{
	DiffuseTarget = vec4(vec3(.5), SpecularCoeff);
	NormalTarget = vec4(normalize(WorldNormal.xyz), SpecularPower);
}

#endif